#!/bin/bash 

make_dir() {
  if ! [ -d "forensic_log_website_daffainfo_log" ]
  then
  `mkdir forensic_log_website_daffainfo_log`
  fi
}

req_per_hour() {
  awk '
  BEGIN {
    n=0
  }
  /^"[0-9]*.[0-9]*.[0-9]*.[0-9]*"/ {++n}
  END {
    print "Rata-rata serangan adalah sebanyak",n/24,"requests per jam"
  }
' log_website_daffainfo.log > ./forensic_log_website_daffainfo_log/ratarata.txt
}

most_ip() {
  awk -F ':' '
    {print $1}
  ' log_website_daffainfo.log | uniq -c | sort -bgr > ./forensic_log_website_daffainfo_log/result.txt
  firstLine=`awk 'NR==1 {print "IP yang paling banyak mengakses server adalah: ",$2,"sebanyak",$1,"requests"}' ./forensic_log_website_daffainfo_log/result.txt`
  echo $firstLine > ./forensic_log_website_daffainfo_log/result.txt
}

user_agent_curl() {
  awk -F ':' '
  BEGIN {
    n=0
  }
  /curl/ {++n}
  END {
    print "Ada",n,"requests yang menggunakan curl sebagai user-agent"
  }
' log_website_daffainfo.log >> ./forensic_log_website_daffainfo_log/result.txt
}

two_am_attacks() {
  awk -F ':' '
  {
    if($3 == "02") {
      print "IP Address",$1,"Jam 2 pagi"
    }
  }
' log_website_daffainfo.log >> ./forensic_log_website_daffainfo_log/result.txt
}

make_dir
req_per_hour
most_ip
user_agent_curl
two_am_attacks