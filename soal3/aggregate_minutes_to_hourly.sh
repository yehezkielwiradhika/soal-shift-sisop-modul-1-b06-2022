#!/bin/bash

currDate=`date '+%Y%m%d%H'`
title="metrics_agg_$currDate"

echo "" > "/home/kali/$title.log"

totfreemem=0
minfreemem=0
maxfreemem=0
avgfreemem=0
totdatamem=0
mindatamem=0
maxdatamem=0
avgdatamem=0

freecounter=0

for filename in /home/kali/*.log;
do
tmp=`awk '
  /Mem/ { printf "%d", $4 }
' $filename`
if ! [ $tmp ]
then
  continue
fi
freecounter="$(($freecounter + 1))"
totfreemem="$(( $totfreemem + $tmp ))"
if [ $tmp -lt $minfreemem ]
then
  minfreemem=$tmp
fi
if [ $tmp -gt $maxfreemem ]
then
  maxfreemem=$tmp
fi
done

datacounter=0

for filename in /home/kali/*.log
do 
tmp=`awk '
  /diskmem/ { printf "%d", $2 }
' $filename`
if ! [ $tmp ]
then
  continue
fi
tmp="$(( $tmp * 1000 ))"
datacounter="$(( $freecounter + 1))"
totdatamem="$(( $totdatamem + $tmp ))"
if [ $tmp -lt $mindatamem ]
then
  mindatamem=$tmp
fi
if [ $tmp -gt $maxdatamem ]
then
  maxdatamem=$tmp
fi

done

avgfreemem=`expr $totfreemem / $freecounter`
avgdatamem=`expr $totdatamem / $datacounter`


echo "total free mem = $totfreemem" >> "/home/kali/$title.log"
echo "minimum free mem = $minfreemem" >> "/home/kali/$title.log"
echo "maximum free mem = $maxfreemem" >> "/home/kali/$title.log"
echo "avg free mem = $avgfreemem">>"/home/kali/$title.log"
echo "total data mem = $totdatamem">>"/home/kali/$title.log"
echo "minimum data mem = $mindatamem">>"/home/kali/$title.log"
echo "maximum data mem = $maxdatamem">>"/home/kali/$title.log"
echo "avg data mem = $avgdatamem">>"/home/kali/$title.log"
