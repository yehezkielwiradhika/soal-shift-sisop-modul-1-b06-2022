#!/bin/bash

currDate=`date '+%Y%m%d%H%M%S'`
title="metrics_$currDate"

echo "instructions:"
echo "1. free -m --> to monitor free ram"
echo "2. du -sh <target_path> --> to monitor size of dir path"
printf "> "

cmd=''
command=''
spec=''
path=''

# read and split string
read command
IFS=' '
read -ra arr <<< "$command"
cmd="${arr[0]}"
spec="${arr[1]}"
path="${arr[2]}"

if [ "$cmd" = "free" ]
then
  echo "freemem: " `$command` > "/home/kali/$title.log"
elif [ "$cmd" = "du" ]
then
  echo "diskmem: " `$command` > "/home/kali/$title.log"
fi
