#!/bin/bash

currDate=`date '+%m/%d/%Y %H:%M:%S'`
currDay=`date '+%Y-%m-%d'`
username=''
password=''

log_func() {
  `echo "$currDate $1" >> log.txt`
}

readPass() {
  prompt="Password: "
  while IFS= read -p "$prompt" -r -s -n 1 char 
  do
  if [[ $char == $'\0' ]];     then
      break
  fi
  if [[ $char == $'\177' ]];  then
      prompt=$'\b \b'
      password="${password%?}"
  else
      prompt='*'
      password+="$char"
  fi
  done
  echo ""
}

login() {
  checkFileFolders
  read -p 'Username: ' username
  readPass

  checkPass
}

checkPass() {
  pass=`awk 'user==$1 {print $2}' user=$username users/user.txt`
  if ! [[ "$pass" = "$password" ]]
  then
    log_func "LOGIN: ERROR Failed login attempt on user $username"
    exit
  else
    log_func "LOGIN: INFO User $username logged in"
  fi
}

checkFileFolders() {
  # check if users folder exists
  if ! [ -d users ]
  then
    mkdir users
  fi

  # check if user file exists
  if ! [ -f users/user.txt ]
  then
    touch users/user.txt
  fi
}

computeLogs() {
  awk "
    /LOGIN/ && /\s$username\s/ { ++n; print }
    END {
      print \"Jumlah percobaan login = \", n
    }" ./log.txt
}

downloadFiles() {
  currDay=`date '+%Y-%m-%d'`
  dirName="$currDay"_$username
  num=0
  if ! [ -d "./$dirName" ]
  then
    `mkdir $currDay\_$username`
  else
    num=`cd ./$dirname && ls ./$dirName | wc -l`
  fi
  for ((i=$num+1; i<=$num+$1; i=i+1))
  do
    if [ $i -lt 10 ]
    then
      wget https://loremflickr.com/320/240 -O ./$dirName/PIC_0$i.jpg
    else
      wget https://loremflickr.com/320/240 -O ./$dirName/PIC_$i.jpg
    fi
  done
  #zip the folder
  zip -P $password -r $dirName.zip $dirName
  rm -rf $dirName
  printf "\n\nSuccessfully downloaded $1 files!"
}

checkZip() {
  currDay=`date '+%Y-%m-%d'`
  dirName="$currDay"_$username
  if [ -f "$dirName.zip" ]
  then
    unzip -P $password $dirName.zip
  else
    mkdir $dirName
  fi
}

askCommand() {
  login
  echo "Commands:"
  echo "1. att"
  echo "2. dl N (N = numbers of images)"
  read -p "> " cmd
  IFS=' '
  cmdarr=($cmd)
  first="${cmdarr[0]}"
  second="${cmdarr[1]}"
  if [ $first == 'att' ]
  then
    computeLogs
    exit
  elif [ $first == "dl" ]
  then
    checkZip
    downloadFiles $second
  fi
}

askCommand