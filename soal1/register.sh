#!/bin/bash

currDate=`date '+%m/%d/%Y %H:%M:%S'`
password=''
username=''

log_func() {
  `echo "$currDate $1" >> log.txt`
}

checkUsername() {
  #check wrong username
  if [[ $username = *" "* ]]
  then
    printf "\nUsername shouldn't contain whitespaces!"
    exit
  fi

  #check user already exist
  LINE=1
  while read -r CURRENT_LINE
    do
      IFS=' '
      arr=($CURRENT_LINE)
      user_in=($arr)
      if [ $username == $user_in ]
      then
        log_func "REGISTER: ERROR User already exist"
        exit
      fi
      ((LINE++))
  done < "./users/user.txt"
}

checkPassword() {
  # check wrong password

  if [[ "$password" != "$username" && ${#password} -ge 8 && "$password" == *[A-Z]* && "$password" == *[a-z]* && "$password" == *[0-9]* && "$password" != *[^0-9a-zA-Z]* ]]
  then
    if [ ! -d "./users" ]
    then
      `mkdir users`
    fi
    `echo $username $password >> ./users/user.txt`
    log_func "REGISTER: INFO User $username registered successfully"
  else
    printf "\nPassword should have the minimum length of 8 and consist of at least one uppercase, numeric, and lowercase value"
    exit
  fi
}

checkFileFolders() {
  # check if users folder exists
  if ! [ -d users ]
  then
    mkdir users
  fi

  # check if user file exists
  if ! [ -f users/user.txt ]
  then
    touch users/user.txt
  fi
}

readPass() {
  prompt="Password: "
  while IFS= read -p "$prompt" -r -s -n 1 char 
  do
  if [[ $char == $'\0' ]];     then
      break
  fi
  if [[ $char == $'\177' ]];  then
      prompt=$'\b \b'
      password="${password%?}"
  else
      prompt='*'
      password+="$char"
  fi
  done
}

run() {
  checkFileFolders
  read -p 'Username: ' username
  checkUsername
  readPass
  checkPassword
}

run