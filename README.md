# soal-shift-sisop-modul-1-B06-2022

## Nama Anggota Kelompok

- Rycahaya Sri Hutomo (5025201046)
- Hesekiel Nainggolan (5025201054)
- Yehezkiel Wiradhika (5025201086)

## Penjelasan Soal #1

Han membuat sebuah program untuk membantu teman-temannya dalam menyelesaikan tugas mencari foto di laptopnya.<br>
<br><strong>Penjelasan poin a:</strong>
<br>Membuat sistem register pada <code>script.sh</code>
<br>Menyimpan username dan password yang berhasil didaftarkan di file <code>./users/user.txt</code>
<br>Membuat sistem login di script <code>main.sh</code>
<br>
<br><strong>Penjelasan poin b:</strong>
<br>Input password di sistem login dan register dibuat tertutup (tidak muncul karakter asli password saat input)
<br>Kriteria password yang didaftarkan :

1. Minimal 8 karakter<br>
2. Memiliki minimal 1 huruf kapital dan 1 huruf kecil<br>
3. Alphanumeric. Karakter yang diinputkan hanya boleh berupa huruf (a sampai z dan A-Z) serta angka (0-9)<br>
4. Tidak boleh sama dengan username (<code>password != username</code>)

<br><strong>Penjelasan poin c:</strong>
<br>Setiap percobaan login dan register dicatat di log.txt
<br>Format catatan percobaan login dan register : <code>MM/DD/YY hh:mm:ss <strong>MESSAGE</strong></code>
<br>Message dibedakan berdasarkan aksi:

1. Register dengan username sudah ada di file <code>./users/user.txt</code>, message log: <code>REGISTER: ERROR User already exist</code><br>
2. Register berhasil (username dan password yang diinputkan sama seperti yang ada di file <code>./users/user.txt</code>), message log : <code>REGISTER: INFO User <strong>USERNAME</strong> registered successfully</code><br>
3. Login dengan password yang salah, message log: <code>LOGIN: ERROR Failed login attempt on user <strong>USERNAME</strong></code><br>
4. Login berhasil, message log: <code>LOGIN: INFO User <strong>USERNAME</strong> logged in</code>

<br><strong>Penjelasan poin d:</strong>
<br>User dapat menginputkan 2 command setelah login berhasil. Command tersebut antara lain:<br>
<br><strong>dl N (N = jumlah gambar yang akan didownload)</strong>

1. Download gambar dari link https://loremflickr.com/320/240 sebanyak N<br>
2. Hasil download dimasukkan ke folder. Format nama folder : <code><strong>YYYY-MM-DD_USERNAME</strong></code><br>
3. Format nama gambar : <code><strong>PIC_XX</strong></code>, dengan nomor yang berurutan. Contoh PIC_01, PIC_02<br>
4. Setelah didownload semua, folder otomatis dizip (format nama sama dengan folder) dan dipassword sesuai password username<br>
5. Jika ada file yang sudah di zip, maka diunzip kemudian ditambahkan gambar, lalu dizip kembali dan di password (sesuai poin 4)

<strong>att</strong>
<br>Menghitung jumlah percobaan login baik yang berhasil maupun tidak berhasil dari user yang sedang login.<br>

## Penyelesaian Soal #1

Mengikuti pertanyaan yang diberikan :<br>

1. Maka langkah awal ialah dengan membuat suatu sistem register pada <code>register.sh</code> yang disimpan pada file <code>./users/user.txt</code> dan sistem login pada scrip <code>main.sh</code><br>
2. Untuk menginputkan password, maka :<br>
   Membuat pengkondisian jika password yang diinputkan minimal 8 karakter , Memiliki minimal 1 huruf kapital , 1 huruf kecil dan Alphanumeric , Tidak boleh sama dengan username, maka akan dilakukan pengkondisian lagi dimana jika tidak terdapat direktori “users” maka akan dibuat directory “users”. Jika sudah terdapat directory “users” dan password yang inputkan benar maka username akan dibuat/dicetak kedalam file <code>./users/user.txt</code>, menggunakan fungsi <code>`echo "$currDate $1" >> log.txt`</code> dan akan ditampilkan <code>REGISTER : INFO User $username registered successfully</code>. Jika penginputan password salah, maka akan ditampilkan <code>REGISTER: ERROR Password should have the minimum length of 8 and consist of at least one uppercase, numeric, and lowercase value</code>.<br>
3. Setiap percobaan login yang dilakukan akan tercatat pada <code>log.txt</code>, dimana dinamai dengan format <code>MM/DD/YY hh:mm:ss</code>, untuk membuat hal tersebut maka akan dibuat fungsi <code>currDate=`date '+%m/%d/%Y %H:%M:%S'`</code> dengan disertai dengan message sebagai berikut :

- Jika register dengan username sudah terdaftar pada <code>log.txt</code> maka akan ditampilkan message : <code>REGISTER: ERROR User already exist</code>, dimana kita membuat fungsi untuk membaca file <code>log.txt</code>. Fungsi tersebut dibuat untuk membaca tiap line pada <code>log.txt</code>.
- Jika percobaan register berhasil maka akan ditampilan message : <code>REGISTER : INFO User $username registered successfully</code>. Dengan syarat bahwa proses penginputan password sudah benar.
- Dalam proses login, akan dibuat suatu fungsi untuk membaca inputan login dan juga membaca directory <code>users</code> dan file <code>log.txt</code>, dimana Jika user login namun password salah maka akan ditampilkan massage : <code>LOGIN: ERROR Failed login attempt on user USERNAME</code>. Untuk mengecek apakah password yang dimasukkan benar atau salah, dibuat fungsi untuk mengecek, jika password yang lama (yang sudah terdaftar pada <code>log.txt</code>) sama dengan password saat login:

```
pass=`awk 'user==$1 {print $2}' user=$username users/user.txt` ·
      if ! [[ "$pass" = "$password" ]]
log_func "LOGIN: ERROR Failed login attempt on user $username
```

- Jika user berhasil login, dimana password dan username tidak salah dan sudah terdaftar pada register, maka akan ditampilkan massage: <code>LOGIN: INFO User USERNAME logged in</code>.
  Dimana akan dicek melalui fungsi :

```
pass=`awk 'user==$1 {print $2}' user=$username users/user.txt`
     if  [[ "$pass" = "$password" ]]
log_func “LOGIN: INFO User USERNAME logged in”
```

4. Setelah berhasil login, maka tahap selanjutnya yaitu membuat fungsi untuk membaca command yang dimasukkan dimana :

- Jika command nya adalah <code>dl N</code> (N = Jumlah gambar yang akan didownload), dimana fungsi command <code>dl N</code> merupakan fungsi untuk mendownload gambar dari https://loremflickr.com/320/240 dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan ke dalam folder dengan format nama <code>YYYY-MM-DD_USERNAME</code>.
  Maka akan dibuat suatu fungsi download, dimana kita definisikan terlebih dahulu:

<code>currDay=`date '+%Y-%m-%d'</code>`, untuk membuat format tanggal untuk format namanya
<code>dirName="$currDay"_$username”</code>, untuk membuat format folder tempat menyimpan fotonya
<code>num=0</code>, untuk menghitung jumlah foto yang didownload

Akan dibuat juga fungsi untuk melihat apakah sudah terdapat folder penyimpanan foto atau belum, jika folder belum ada maka akan dibuat folder <code>$currDay\_$username</code>

```
If ! [-d “./$dirName”]
then
    ‘mkdir $currDay\_$username’
```

<br>Untuk proses mendownload akan dibuat fungsi perulangan, dimana kita akan mendownload foto dengan syntax wget sebanyak N yang kita masukkan:

```
for ((i=$num+1; i<=$num+$1; i=i+1))
do
    If [ $i -lt 10]
  	         then
  		 wget
 	https://loremflickr.com/320/240 -O./$dirName/PIC_0$i.jpg
  else
Wget https://loremflickr.com/320/240-O./$dirName/PIC_$i.jpg
```

<br>Setelah prose mendownload selesai, maka akan disimpan ke dalam sebuah file zip. Untuk itu maka akan dibuat fungsi terlebih dahulu untuk membuat file zip :
Karena file akan dibuat dalam penamaan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut,maka akan didefinisikan terlebih dahulu <code>currDay</code> :<br>

```
currDay= ‘date ‘+%Y-%m-%d’’
  dirName=”$currDay”_$username
```

<br>Kemudian akan dicek kembali, apakah penamaan zip tersebut sudah ada atau tidak, jika sudah file zip yang sudah ada akan di unzip terlebih dahulu :<br>

```
If [ -f “$dirName.zip”]
   then
     Unzip -p $password $dirName.zip
   else
      echo “test”
	mkdir $dirName
```

<br>
Jika command yang diinputkan adalah <code>att</code>, maka akan dihitung jumlah percobaan login, baik yang berhasil maupun tidak. Untuk menghitung lah tersebut, akan dibuat suatu fungsi dimana akan membaca file code>log.txt</code>, dengan syntax sebagai berikut :

```
	awk “
		/LOGIN/ && /$username/ { ++n; print }
		END 	{
			Print \”Jumlah percobaan login = \”, n
			}” ./log.txt
```

## Penjelasan Soal #2

Pada soal 2, kita diminta untuk sebuah script awk bernama <code>soal2_forensic_dapos.sh</code>, untuk membaca log website https://daffa.info. Hal -hal yang harus dikerjakan ialah sebagai berikut :<br>
<br>a. Kita diminta untuk membuat suatu folder yang bernama <code>forensic_log_website_daffainfo_log</code>, dimana semua hasil pengerjaan akan dikumpul pada folder ini.<br>

<br>b. Kemudian kita diminta untuk menghitung berapa rata-rata request per jam yang dikirimkan penyerang ke website. Hasil rata-ratanya akan dimasukkan ke dalam sebuah file bernama <code>ratarata.txt</code>, dimana file <code>ratarata.txt</code> berada pada folder <code>forensic_log_website_daffainfo_log</code><br>

<br>c. Kita diminta untuk menampilkan IP yang paling banyak melakukan request ke server dan menampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Hasil dari proses tersebut akan dimasukkan kedalam file <code>result.txt</code>, dimana file <code>result.txt</code> tersebut juga berada pada folder <code>forensic_log_website_daffainfo_log</code><br>

<br>d. Kita diminta untuk menghitung banyak requests yang menggunakan user-agent curl. Hasil dari perhitungan tersebut akan dimasukkan kedalam sebuah file bernama result.txt dimana file <code>result.txt</code> dimana file tersebut juga berada pada folder <code>forensic_log_website_daffainfo_log</code><br>

<br>e. Pada soal selanjutnya, kita diminta untuk mencari tahu daftar IP yang mengakses website pada jam 2 pagi pada tanggal 23. Hasil dari pencarian akan dimasukkan kedalam file <code>result.txt</code> dimana file tersebut juga berada pada folder <code>forensic_log_website_daffainfo_log</code><br>

## Penyelesaian Soal 2:

Pertama kita membuat fungsi untuk membuat folder terlebih dahulu bernama forensic_log_website_daffainfo_log

```
make_dir() {
  if ! [ -d "forensic_log_website_daffainfo_log" ]
  then
  `mkdir forensic_log_website_daffainfo_log`
  fi
}
```

Di sini kita mengecheck apakah folder sudah terdapat dalam file dengan menggunakan

```
if ! [ -d "forensic_log_website_daffainfo_log" ]
```

Jika ternyata tidak ada, maka kita membuat folder tersebut

```
`mkdir forensic_log_website_daffainfo_log`
```

Kemudian kita mendefinisikan fungsi untuk menghitung berapa request yang terjadi dalam satu jam

```
req_per_hour() {
  awk '
  BEGIN {
    n=0
  }
  /^"[0-9]*.[0-9]*.[0-9]*.[0-9]*"/ {++n}
  END {
    print "Rata-rata serangan adalah sebanyak",n/24,"requests per jam"
  }
' log_website_daffainfo.log > ./forensic_log_website_daffainfo_log/ratarata.txt
}
```

Di sini kita menggunakan program awk untuk menentukan pola request yang ditandai dengan adanya ip address di awal tiap-tiap line kemudian berdasarkan pola yang ditemukan, kita menambahkan n untuk masing-masing baris dengan menggunakan:

```
    /^"[0-9]*.[0-9]*.[0-9]*.[0-9]*"/ {++n}
```

Sebelum kita melakukan hal itu, kita harus mendefinisikan n terlebih dahulu dengan

```
BEGIN {
    n=0
}
```

BEGIN menandakan bahwa n didefinisikan terlebih dahulu sebelum melakukan kode awk selanjutnya. Setelah itu, di akhir iterasi awk, kita melakukan output jumlah request dengan menggunakan:

```
END {
    print "Rata-rata serangan adalah sebanyak",n/24,"requests per jam"
}
```

Kita meread dari log_website_daffainfo.log kemudian melakukan output ke forensic_log_website_daffainfo_log/ratarata.txt dengan kode sebagai berikut:

```
log_website_daffainfo.log > ./forensic_log_website_daffainfo_log/ratarata.txt
```

Kemudian kita mendefinisikan fungsi untuk menghitung ip address dengan jumlah paling banyak melakukan request.

```
most_ip() {
awk -F ':' '
    {print $1}
' log_website_daffainfo.log | uniq -c | sort -bgr > ./forensic_log_website_daffainfo_log/result.txt
firstLine=`awk 'NR==1 {print "IP yang paling banyak mengakses server adalah: ",$2,"sebanyak",$1,"requests"}' ./forensic_log_website_daffainfo_log/result.txt`
echo $firstLine > ./forensic_log_website_daffainfo_log/result.txt
}
```

Kita menggunakan program awk untuk menemukan ip dengan jumlah request terbanyak.
Pertama kita melakukan pemisahan parameter berdasarkan “:” dengan kode:

```
awk -F ':'
```

Kita mengambilnya dari log_website_daffainfo.log kemudian kita melakukan penghitungan jumlah dengan ip unik kemudian kita melakukan sorting dengan menggunakan kode sebagai berikut:

```
log_website_daffainfo.log | uniq -c | sort -bgr >
```

Tujuan dari sorting adalah untuk menampilkan ip dan jumlah ip paling banyak di awal line, sehingga kita hanya perlu mengambil line pertama saja. Setelah itu kita mengambil line pertama (ip dan jumlah request paling banyak) lalu memasukkannya di result.txt dengan kode sebagai berikut:

```
echo $firstLine > ./forensic_log_website_daffainfo_log/result.txt
```

Kemudian, kita mendefinisikan fungsi yang menghitung banyaknya request yang menggunakan user-agent “curl”:

```
user_agent_curl() {
    awk -F ':' '
    BEGIN {
        n=0
    }
    /curl/ {++n}
    END {
        print "Ada",n,"requests yang menggunakan curl sebagai user-agent"
    }
    ' log_website_daffainfo.log >> ./forensic_log_website_daffainfo_log/result.txt
}
```

Pertama kita memisahkan tiap-tiap baris dengan “:” menggunakan awk dengan kode:

```
awk -F ':'
```

Selanjutnya, pada program awk tersebut, pertama-tama kita mendefinisikan n sebagai nol sehingga kita bisa increment untuk masing-masing request dengan user-agent curl

```
BEGIN {
    n=0
}
/curl/ {++n}
```

Kemudian, di akhir kode awk, kita mengoutput hasil jumlah request dengan user-agent curl

```
END {
    rint "Ada",n,"requests yang menggunakan curl sebagai user-agent"
}
```

Lalu memasukkan hasil tersebut ke dalam result.txt

```
' log_website_daffainfo.log >> ./forensic_log_website_daffainfo_log/result.txt
```

Fungsi terakhir yang kita definisikan adalah untuk mengetahui ip address apa saja yang ada pada jam 2 pagi:

```
two_am_attacks() {
    awk -F ':' '
    {
        if($3 == "02") {
            print "IP Address",$1,"Jam 2 pagi"
        }
    }
    ' log_website_daffainfo.log >> ./forensic_log_website_daffainfo_log/result.txt
}
```

Pertama, kita menggunakan program awk untuk memisahkan masing-masing baris dengan “:”

```
awk -F ':'
```

Kemudian kita menlakukan pengecheckan jika parameter ke-3 adalah angka 02, maka ia mengindikasikan bahwa request tersebut terjadi pada pukul 2. Jika ya, maka kita output hasilnya.

```
if($3 == "02") {
    print "IP Address",$1,"Jam 2 pagi"
}
```

Hasil dioutputkan dari log_website_daffainfo.log ke dalam result.txt

```
' log_website_daffainfo.log >> ./forensic_log_website_daffainfo_log/result.txt
```

Setelah itu, kita melakukan pemanggilan fungsi dengan kode sebagai berikut:

```
make_dir
req_per_hour
most_ip
user_agent_curl
two_am_attacks
```

<br>

## Penjelasan Soal 3:

di sini kita diminta untuk membuat program monitoring resource yang tersedia pada komputer.

### hal yang perlu kita buat:

- monitoring ram (free -m)
- monitoring size directory (du -sh <target_path>)
- mencatat hasil yang didapatkan dari kedua hal tersebut

#### note

target path yang dimonitor terdapat pada
/home/{user}/.

<br>
a. Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log.
{YmdHms}. 
<br>
b. Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap
menit.<br>
c. Buat satu script untuk membuat agregasi file log ke satuan jam. Script
agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file
agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap
metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis.<br>
d. Karena file log bersifat sensitif semua file log hanya dapat dibaca oleh user
pemilik file.

## Penjelasan Jawaban 3:

### pada minute_log.sh

1. pertama kita mendefinisikan waktu, setelah itu berdasarkan waktu tersebut, kita membuat file sesuai format yang diminta soal.
2. setelah itu kita menuliskan instructions dan meminta input dari user
3. kita split command menjadi beberapa bagian, yakni cmd, spec, dan path. (cmd adalah command awal \[free atau du\], spec adalah argumen, kemudian path adalah target_path)
4. kita memasukkan file masing-masing berdasarkan argumen cmd
5. membuat crontab secara manual untuk merun script secara otomatis setiap 1 menit

### pada aggregate_minutes_to_hourly.sh

1. kita mendefinisikan waktu dan membuat nama sesuai format soal
2. mendefinisikan total, minimum, maximum, dan rata-rata bagi freemem dan datamem
3. untuk masing-masing file yang ada di /home/{user}, kita menloop berdasarkan extension .log dan melakukan operasi di masing-masing soal ke variable yang telah kita tentukan sebelumnya
4. kita mengoutput hasilnya ke file yang telah kita buat
5. membuat crontab secara manual untuk merun script secara otomatis setiap 1 jam

## Kendala dalam pengerjaan soal :

Tidak ada kendala saat pengerjaan soal
